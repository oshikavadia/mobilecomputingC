package com.example.shane.mobilecomputingappc;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.Handler;
import android.os.IBinder;
import android.os.Message;
import android.os.Messenger;
import android.os.RemoteException;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {
    Messenger mService = null;
    boolean mIsBound;
    TextView mCallbackText;
    final Messenger mMessenger = new Messenger(new IncomingHandler());
    static final int MSG_REGISTER_CLIENT = 1;
    static final int MSG_UNREGISTER_CLIENT = 2;
    static final int MSG_SET_VALUE = 3;
    Intent serviceIntent;
    int mValue = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        mCallbackText = findViewById(R.id.callBackText);
        serviceIntent = new Intent();
//        serviceIntent.setPackage("com.oshikavadia.mobiledevpartb");
        serviceIntent.setComponent(new ComponentName("com.oshikavadia.mobiledevpartb", "com.oshikavadia.mobiledevpartb.RemoteMessengerService"));

    }

    @Override
    protected void onStart() {
        super.onStart();
        bindService(serviceIntent, mConnection, Context.BIND_AUTO_CREATE);

    }

    @Override
    protected void onStop() {
        super.onStop();
        if(mIsBound)
            unbindService(mConnection);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
//        Toast.makeText(MainActivity.this, "Service Un-Binded", Toast.LENGTH_LONG).show();
    }

    public void buttonOnClick(View view) {
        EditText editText = (EditText) findViewById(R.id.sendText);
        String input = editText.getText().toString();
        if (!mIsBound) return;
        Bundle bundle = new Bundle();
        bundle.putString("string: ", input);
        Message m = Message.obtain(null, + MSG_SET_VALUE, mValue, 0);
        m.setData(bundle);
        try {
            mService.send(m);
        } catch (RemoteException e) {
            e.printStackTrace();
        }

    }

    class IncomingHandler extends Handler {
        @Override
        public void handleMessage(Message msg) {
            switch (msg.what) {
                case MSG_SET_VALUE:
                    mCallbackText.setText("Received from service: " + msg.getData().getString("string"));
                    break;
                default:
                    super.handleMessage(msg);
            }
        }
    }

    private ServiceConnection mConnection = new ServiceConnection() {

        @Override
        public void onServiceConnected(ComponentName name, IBinder service) {
            Log.d("ServiceConnection", "on service connected");
            mService = new Messenger(service);
            mCallbackText.setText("Attached.");
            mIsBound = true;
            try {
                Message msg = Message.obtain(null,
                        MSG_REGISTER_CLIENT);
                msg.replyTo = mMessenger;
                mService.send(msg);

                // Give it some value as an example.
                msg = Message.obtain(null,
                        MSG_SET_VALUE, this.hashCode(), 0);
                mService.send(msg);
            } catch (RemoteException e) {
                // In this case the service has crashed before we could even
                // do anything with it; we can count on soon being
                // disconnected (and then reconnected if it can be restarted)
                // so there is no need to do anything here.
            }

            // As part of the sample, tell the user what happened.
            Toast.makeText(getApplicationContext(), "remote service bound",
                    Toast.LENGTH_SHORT).show();
        }

        @Override
        public void onServiceDisconnected(ComponentName name) {
            mService = null;
            mIsBound = false;
            mCallbackText.setText("Disconnected.");
            Toast.makeText(getApplicationContext(), "remote service unbound",
                    Toast.LENGTH_SHORT).show();
        }
    };
}
